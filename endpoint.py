from os import getenv
from socket import gethostbyaddr
from requests import get, post
from flask import Flask, request, render_template

def resolve_external_url():
    ip = get('https://checkip.amazonaws.com').text.strip()
    host = gethostbyaddr(ip)[0]
    port = int(getenv('EXTERNAL_PORT', getenv('FLASK_RUN_PORT', '5000')))
    return f'http://{host}:{port}/endpoint'

external_url = getenv('EXTERNAL_URL', resolve_external_url())
hello_message = getenv('HELLO_MESSAGE', 'some example text')

app = Flask(__name__)

@app.route('/')
def index():
    return render_template('index.html', endpoint_url=external_url)

@app.route('/endpoint', methods=['POST'])
def endpoint():
    print(f'Request from {request.remote_addr}: {request.json}')
    url = request.json['url']
    res = post(url, json={'hello': hello_message})
    print(f"Response from {url}: '{res.text}'")
    return {'response': res.text}

def main():
    print(f'API is reachable at: {external_url}')
    app.run(host=getenv('FLASK_RUN_HOST', '0.0.0.0'), port=int(getenv('FLASK_RUN_PORT', '5000')))

if __name__ == '__main__':
    main()
